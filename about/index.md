---
layout: layouts/base.njk
title: About
templateClass: tmpl-post
eleventyNavigation:
  key: About
  order: 3
---

Built by <a href="https://noagendasocial.com/@noagendashowvideo">smallygdala</a>.

Source <a href="https://gitlab.com/smallygdala/na_wordle">code</a>.